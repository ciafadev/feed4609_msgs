| Msg             | Description                                                                                                                                                                                                                              |
|-----------------|------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| GimbalTelemetry | Telemetry fields from STANAG4609. Also contains a MPEG PTS field for synchronization with vidro frames. Contains messages for gimbal and platform telemetry.                                                                             |
| Mpeg2Frame      | Contains a sensor_msgs/Image message with decoded image. Has a MPEG PTS field for synchronization with telemetry. The video frame can be IR or visible spectrum. After synchronization, that information is contained in the telemetry. |
| Sensor | Telemetry fields concerning the gimbal and sensor. Not all fields form the standard are implemented.|
| Platform | Telemetry fields concerning the UAV. Not all fields form the standard are implemented. |
| SyncedFrame4609 | A (GimbalTelemetry, sensor_msgs(Image) tuple to be compiled after synchronization. |




More information regarding telemetry data fields exists in the "UAS Datalink Local Set", [MISB ST 0601.8](https://upload.wikimedia.org/wikipedia/commons/1/19/MISB_Standard_0601.pdf) standard..